#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#define MAX 255

typedef struct{
    char rua[MAX], bairro[MAX], cidade[MAX], siglaEstado[2];
}Endereco;

typedef struct{
    char matricula[MAX];
    char sexo;
    char nome[MAX];
    Endereco endAluno;
}Aluno;

//declaracao de funcoes

void mostraMenu(void);
void pausaParaSair(void);
void cadastraAluno(void);
void excluiAluno(void);
void insereDados(void);
void atualizaDado(void);
void imprimeAlunos(void);

//fim declaracao de funcoes

Aluno *lista;
FILE *saidaImpressao;
int quantidade, i=0, contadorProcura;

void main(void){ //programa principal
    setlocale(LC_ALL, "Portuguese");
    int opcao;
    printf("Entre com a quantidade de alunos: ");
    scanf("%d", &quantidade);
    lista = (Aluno *)malloc(quantidade*sizeof(Aluno));
    opcao = 0;
    system("cls");
    while(opcao!=5){
        mostraMenu();
        scanf("%d", &opcao);
        system("cls");
        if(opcao == 1){
            cadastraAluno();
        }
        else if(opcao == 2){
            excluiAluno();
        }
        else if(opcao == 3){
            atualizaDado();
        }
        else if(opcao == 4){
            imprimeAlunos();
        }
    }
    free(lista);
    pausaParaSair();
    exit(EXIT_SUCCESS);
}

void pausaParaSair(void){
    fflush(stdin);
    printf("Pressione <enter> para continuar...");
    getchar();
    system("cls");
}

void mostraMenu(void){
    setlocale(LC_ALL, "Portuguese");
    printf("**********CADASTRO DE ALUNOS**********\n\n\n");
    printf("\t1. Cadastrar um aluno.\n");
    printf("\t2. Excluir um aluno.\n");
    printf("\t3. Atualizar dados do aluno.\n");
    printf("\t4. Imprimir a lista de alunos.\n");
    printf("\t5. Sair do programa.\n\n");
    printf("\t\tSua escolha (digite o NMERO correspondente): ");
}

void cadastraAluno(void){
    setlocale(LC_ALL, "Portuguese");
    if(i>quantidade){
        printf("Impossvel cadastrar mais alunos! Lista cheia!\n\n");
    }
    else{
        insereDados();
        i++;
    }
    pausaParaSair();
}

void excluiAluno(void){
    setlocale(LC_ALL, "Portuguese");
    char matriculaEntrada[MAX];
    printf("Entre com a MATRCULA do ALUNO: ");
    fflush(stdin);
    gets(matriculaEntrada);
    for(contadorProcura=0; contadorProcura<i; contadorProcura++){
        if(matriculaEntrada == lista[i].matricula){
            lista[contadorProcura].nome[0] = '\0';
            lista[contadorProcura].sexo = '\0';
            lista[contadorProcura].matricula[0] = '\0';
            lista[contadorProcura].endAluno.bairro[0] = '\0';
            lista[contadorProcura].endAluno.cidade[0] = '\0';
            lista[contadorProcura].endAluno.rua[0] = '\0';
            lista[contadorProcura].endAluno.siglaEstado[0] = '\0';
            printf("Limpeza concluda com sucesso!\n");
        }
        else{
            printf("Aluno invlido!\n");
        }
    }
    pausaParaSair();
}

void insereDados(void){
    printf("Entre com o NOME: ");
    fflush(stdin);
    gets(lista[i].nome);
    printf("Matrcula: ");
    fflush(stdin);
    gets(lista[i].matricula);
    printf("Sexo: ");
    fflush(stdin);
    lista[i].sexo = getchar();
    printf("\tEndereo:\n");
    printf("*Rua: ");
    fflush(stdin);
    gets(lista[i].endAluno.rua);
    printf("*Bairro: ");
    fflush(stdin);
    gets(lista[i].endAluno.bairro);
    printf("*Cidade: ");
    fflush(stdin);
    gets(lista[i].endAluno.cidade);
    printf("UF (sigla): ");
    fflush(stdin);
    gets(lista[i].endAluno.siglaEstado);
}

void atualizaDado(void){
    setlocale(LC_ALL, "Portuguese");
    char matriculaEntrada[MAX];
    fflush(stdin);
    printf("Entre com a matrcula: ");
    fflush(stdin);
    gets(matriculaEntrada);
    for(contadorProcura=0; contadorProcura<i; contadorProcura++){
        if(matriculaEntrada == lista[contadorProcura].matricula){
            insereDados();
        }
    }
    pausaParaSair();
}

void imprimeAlunos(void){
    setlocale(LC_ALL, "Portuguese");
    saidaImpressao = fopen("Lista-dos-Alunos.txt", "wt");
    if(saidaImpressao == NULL){
        printf("Problema ao CRIAR o arquivo");
    }
    else{
        fprintf(saidaImpressao, "**************ALUNOS CADASTRADOS**************\n\n\n");
        for(contadorProcura=0; contadorProcura<i; contadorProcura++){
            fprintf(saidaImpressao, "**********************************************\n");
            fprintf(saidaImpressao, "                   ALUNO %d\n", contadorProcura+1);
            fprintf(saidaImpressao, "\tNOME: %s\n", lista[contadorProcura].nome);
            fprintf(saidaImpressao, "\tSEXO: %c\n", lista[contadorProcura].sexo);
            fprintf(saidaImpressao, "\tMATRCULA: %s\n", lista[contadorProcura].matricula);
        }
        printf("Lista IMPRESSA COM SUCESSO!\n");
    }
    fclose(saidaImpressao);
    pausaParaSair();
}
